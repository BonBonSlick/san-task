<?php require_once('db_connect.php'); ?>
<DOCTYPE html />
<html>
<head>
	<title>Test HTML</title>
	<script type="text/javascript" src="jquery.js"></script>
	<style>
		html, body{
			padding: 0;
			margin: 0;
		}
	</style>
</head>
<body>

	<a href="/">BACK</a><br>
 

	<h2>Add Menu Part</h2>
	<form action="" method="POST" class="add_menus_here">
		Name
		<input type="text" name="part_name">
		Parent
		<select name="parent"  >
			<option value="0" selected>None</option>
			<?php $data = $db->query('SELECT * FROM `menus_parts`')->fetchAll( );?>
			<?php foreach ($data as $value): ?>
				<option value="<?php echo $value['part_id']; ?>"> <?php echo $value['name']; ?></option>
			<?php endforeach ?>
		</select>
		<button type="submit" name="submit_menu">Add Menu</button>
	</form>


	<?php 

// When New Menu Submits
	if ( isset($_POST['submit_menu']) && !empty( $_POST['part_name']  ) ){
		try {
			$db_con = $db->prepare('INSERT INTO menus_parts ( part_id, name, parent ) VALUES ( part_id, :name, :parent ) ');
			$db_con->execute(array(
				':name' => $_POST['part_name'],
				':parent' => $_POST['parent'],
				));
		} catch (PDOException $e) {
			$e->getMessage();
		}
		header('Location: /');
	} 

	function ShowTree( $ParentID ) { 
		global $db;
		$db_con  = $db->query( "SELECT * FROM  menus_parts WHERE parent=".$ParentID." ORDER BY name");
		if ($db_con > 0) {
			echo("<ul>\n");
			while ( $row = $db_con->fetch() ) {
				$part_id = $row["part_id"];
				$part_name = $row['name'];
				echo    "<li>$part_name</li>";
				ShowTree($part_id ); 
			}
			echo("</ul>\n");
		}
	}
	echo "<b>Menu Parts</b>";
	ShowTree(0); 



	?>







</body>
<footer style="text-align:center;position: absolute; bottom: 3%; height: 25px; width: 100%;">
	<hr>
	<b>Footer</b>
</footer>
</html>




