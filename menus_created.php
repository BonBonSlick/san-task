<?php require_once('db_connect.php'); ?>
<DOCTYPE html />
<html>
<head>
	<title>Test HTML</title>
	<script type="text/javascript" src="jquery.js"></script>
	<style>
		html, body{
			padding: 0;
			margin: 0;
		}
		#add_part:active{
			background: gray;
		}
		#add_part, .remove_part{
			height: 25px;
			width: 125px;
			border: 1px solid rgba(0, 0, 0, 1.0);
			border-radius: 5px;
			color: white;
			display: flex;
			justify-content: center;
			align-items: center;
			margin: 15px  0 15px 0;
			cursor: pointer;
		}
		#add_part{
			background: green;

		}
		.remove_part{
			background: red;
		}
	</style>
</head>
<body>
	<!-- Clone script -->
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(document).on('click','#add_part',function(event){
				// alert('AAA');
				$('.clone_block').first().clone().appendTo('.clone_here').css({display: 'block'});;
			});
			$(document).on('click', 'form .remove_part', function(event) {
				$('.remove_part').parent('form .clone_block').first().remove();
			});
		});
	</script>
	<!-- Block to clone -->
	<div class="clone_block" style="display: none">
		<br>Menu Parts
		<select name="parts[]"  >
			<option value="0" selected>None</option>
			<?php $data = $db->query('SELECT * FROM `menus_parts`')->fetchAll( );?>
			<?php foreach ($data as $value): ?>
				<option value="<?php echo $value['part_id']; ?>"> <?php echo $value['name']; ?></option>
			<?php endforeach ?>
		</select> 
		<div class="remove_part" > -- Remove Part</div>
	</div>  
	<!-- Add menu form -->
	<input  class="menus" type="text" name="menus[]"  style="display: none;"> 	
	<form action="" method="POST" class="clone_parts_here">
		Menu Name
		<input  type="text" name="menu_name"> 
		<div class="clone_here"></div>
		<br>Menu Parts
		<select name="parts[]"  >
			<option value="0" selected>None</option>
			<?php $data = $db->query('SELECT * FROM `menus_parts`')->fetchAll( );?>
			<?php foreach ($data as $value): ?>
				<option value="<?php echo $value['part_id']; ?>"> <?php echo $value['name']; ?></option>
			<?php endforeach ?>
		</select> 


		<br> 
		<div id="add_part">Add Part++</div>
		<button type="submit" name="add_menu">Submit Menu</button>
	</form>



	<?php 


// When New Menu Submits
	if ( isset($_POST['add_menu']) && !empty( $_POST['menu_name']  ) ){
		try {
			$db_con = $db->prepare('INSERT INTO menus ( menu_id, name, parts ) VALUES ( menu_id, :name, :parts ) ');
			$db_con->execute(array(
				':name' => $_POST['menu_name'],
				':parts' => json_encode($_POST['parts']),
				));

		} catch (PDOException $e) {
			$e->getMessage();
		}

		header('Location: /');
	} 




	?>







</body>
<footer style="text-align:center;position: absolute; bottom: 3%; height: 25px; width: 100%;">
	<hr>
	<b>Footer</b>
</footer>
</html>




