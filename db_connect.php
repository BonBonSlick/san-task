<?php
ob_start();
session_start();
  
ini_set('session.gc_maxlifetime', 1);
ini_set('session.cookie_lifetime', 1);
 
define('DBHOST', 'localhost');
define('DBUSER', 'admin');
define('DBPASS', 'admin');
define('DBNAME', 'tests');

try {
	$optimize = array(
		PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
		);
	$db = new PDO ('mysql:host=' . DBHOST . ';dbname=' . DBNAME, DBUSER, DBPASS, $optimize);
} catch (PDOException $e) {
	echo "Some ERROR with connecting to DataBase!!!" . $e->getMessage();
}
?>